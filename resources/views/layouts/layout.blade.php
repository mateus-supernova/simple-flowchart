<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Flowchart</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    @include('layouts.topnav')
    @include('layouts.sidebar')
    @yield('content')
    <script src="/js/flowchart.js"></script>
</body>
</html>